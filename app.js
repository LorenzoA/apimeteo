const weatherIcons = {
    Rain: "wi wi-day-rain",
    Clouds: "wi wi-day-cloudy",
    Clear: "wi wi-day-sunny",
    Snow: "wi wi-day-snow",
    Mist: "wi wi-day-fog",
    Fog: "wi wi-day-fog",
    Drizzle: "wi wi-day-sleet",
    Thunderstorm: "wi wi-day-thunderstorm",
};


async function main(withIP = true) {
    let ville;
    let timeZ;
    if (withIP) {

        const ip = await fetch(
            "https://api.ipdata.co?api-key=1ac10293debdff28e40d452bea5e11c66c1480ae7421de6346089cf2"
        )
            .then((reponse) => reponse.json())
            .then((json) => json.ip);

        ville = await fetch(
            "https://api.ipdata.co?api-key=1ac10293debdff28e40d452bea5e11c66c1480ae7421de6346089cf2"
        )
            .then((reponse) => reponse.json())
            .then((json) => json.city);
    } else {

        ville = document.querySelector("#ville").textContent;
    }


    const meteo = await fetch(
        `http://api.openweathermap.org/data/2.5/weather?q=${ville}&appid=f39056e4fcb105c87d66168eb3961b23&lang=fr&units=metric`
    )
        .then((reponse) => reponse.json())
        .then((json) => json);
    //resultats
    console.log(meteo);
    //test time zone
    const coordLat = await fetch(
        `http://api.openweathermap.org/data/2.5/weather?q=${ville}&appid=f39056e4fcb105c87d66168eb3961b23&lang=fr&units=metric`
    )
        .then((reponse) => reponse.json())
        .then((json) => json.coord.lat);
    //resultats
    console.log(coordLat);

    const coordLon = await fetch(
        `http://api.openweathermap.org/data/2.5/weather?q=${ville}&appid=f39056e4fcb105c87d66168eb3961b23&lang=fr&units=metric`
    )
        .then((reponse) => reponse.json())
        .then((json) => json.coord.lon);
    //resultats
    console.log(coordLon);

    timeZ = await fetch(`https://api.ipgeolocation.io/timezone?apiKey=00b5caea23a84de59d7df03e28f65fe5&lat=${coordLat}&long=${coordLon}`)
        .then((reponse) => reponse.json())
        .then((json) => json)
    console.log(timeZ);

    //affichage des eléments grace à cette fonction.
    displayTime(timeZ);
    displayWeatherInfos(meteo);
}
//création de variables pour les éléments de l'api
function displayWeatherInfos(data) {
    const name = data.name;
    const temp = data.main.temp;
    const conditions = data.weather[0].main;
    const description = data.weather[0].description;
    const wind = data.wind.speed;
    const hygro = data.main.humidity;
    // const coordLon = data.coord.lon;
    // const coordLat = data.coord.lat;

    document.querySelector("#ville").textContent = name;
    document.querySelector("#temperature").textContent = temp;
    document.querySelector("#conditions").textContent = description;
    document.querySelector("i.wi").className = weatherIcons[conditions];
    document.querySelector("#wind").textContent = wind;
    document.querySelector("#hygro").textContent = hygro;
}


function displayTime(data) {
    const date = data.date_time_txt;
    document.querySelector("#date").textContent = date;
}

//On rend le span cliquable et modifiable
const ville = document.querySelector("#ville");
ville.addEventListener("click", () => {
    ville.contentEditable = true;

});
ville.addEventListener("keydown", (e) => {
    if (e.keyCode === 13) {
        e.preventDefault();
        ville.contentEditable = false;
        main(false);
    }

})
main();
